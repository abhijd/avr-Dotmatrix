#include<avr/io.h>
#include<math.h>
#include<util/delay.h>

#define userIn PINA
#define dotside1 PORTB
#define dotsideA PORTD

#define userIn_init DDRA
#define dotside1_init DDRB
#define dotsideA_init DDRD

void disp(unsigned char,int);
unsigned char letter[8][8];

int main(void)
{
   
   userIn_init=0x00;
   dotside1_init=0xFF;
   dotsideA_init=0xFF;
   

   letter[0][0]=0b10001000;
   letter[0][1]=0b10010100;
   letter[0][2]=0b10100010;
   letter[0][3]=0b11111111;
   letter[0][4]=0b11000000;
   letter[0][5]=0b10100100;
   letter[0][6]=0b00011100;

   letter[1][0]=0b01000000;
   letter[1][1]=0b00101000;
   letter[1][2]=0b01001000;
   letter[1][3]=0b00110100;
   letter[1][4]=0b10000010;
   letter[1][5]=0b11111111;
   letter[1][6]=0b10000000;

   letter[2][0]=0b00001000;
   letter[2][1]=0b01110000;
   letter[2][2]=0b01000000;
   letter[2][3]=0b00100000;
   letter[2][4]=0b10010000;
   letter[2][5]=0b11111111;
   letter[2][6]=0b10000000;

   letter[3][0]=0b01100100;
   letter[3][1]=0b10011100;
   letter[3][2]=0b10010100;
   letter[3][3]=0b00000010;
   letter[3][4]=0b00000001;
   letter[3][5]=0b11111111;
   letter[3][6]=0b10000000;

   letter[4][0]=0b00011100;
   letter[4][1]=0b10000010;
   letter[4][2]=0b01000001;
   letter[4][3]=0b01111001;
   letter[4][4]=0b10100101;
   letter[4][5]=0b11001001;
   letter[4][6]=0b00010010;
   letter[4][7]=0b00001100;

   letter[5][0]=0b00000000;
   letter[5][1]=0b10000000;
   letter[5][2]=0b11111110;
   letter[5][3]=0b10100001;
   letter[5][4]=0b10010010;
   letter[5][5]=0b10001100;
   letter[5][6]=0b10000000;

   letter[6][0]=0b00000000;
   letter[6][1]=0b11110000;
   letter[6][2]=0b01010100;
   letter[6][3]=0b00100100;
   letter[6][4]=0b01001010;
   letter[6][5]=0b01010010;
   letter[6][6]=0b00100001;
   letter[6][7]=0b00000000;

   letter[7][0]=0b10000100;
   letter[7][1]=0b10110010;
   letter[7][2]=0b11001010;
   letter[7][3]=0b10010010;
   letter[7][4]=0b11001100;
   letter[7][5]=0b10100000;
   letter[7][6]=0b00111100;
   letter[7][7]=0b01000010; 

   while(1)
  {
   if(userIn==1)
	  disp(1,0);
	  
   else if (userIn==2)
      disp(2,1);

   else if(userIn==4)
      disp(4,2);

   else if(userIn==8)
      disp(8,3);

   else if(userIn==16)
      disp(16,4);

   else if(userIn==32)
      disp(32,5);

   else if(userIn==64)
      disp(64,6);

   else if(userIn==128)
      disp(128,7);

   else
   	{
	dotside1=0x00;
	dotsideA=0x00;
	}
   }
   return(1);
}


void disp(unsigned char check,int lnum)
{
    
   while(userIn==check)
      {
	  int j=0;
	  for (int i=1;i<256;i=i*2)
	     {
		 dotside1=i;
		 dotsideA=letter[lnum][j];
		 _delay_ms(5);
		 dotside1=0;
		 j=j+1;
		 }
	  }
}
