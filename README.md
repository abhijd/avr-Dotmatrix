# Dot matix control with AVR Atmega32

A 8x8 dot matrix is conrolled with AVR Atmega32 MCU. First 8 Bengali letters are displayed on the dot matrix.

## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Proteus Design Suite
 

### How-to-simulate
 
 * Clone the repo in your local directory
 * Start Proteus Design Suite
 * In Proteus, open either hw2.DSN 
 * When the design opens, double-click on the Atmega32 microcontroller that opens the "edit component" pop-up
 * From the edit component, add the location of the hw2.hex file in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!
 * Once started, you can change the letters using the switch group (SW1)
 
## Changing the program code

  * To change the program code, you may use AVR Studio/WinAVR to open the hw2.c file and change as desired.
  * Once changed, compile into hex file again using AVR Studio/ WinAVR, and load it again to the Atmega32 following the steps from "How-to-simulate"
 
## Program Flow Diagram

![Flow Diagram](flow-diagram.png "Flow Diagram") 
  
## Hardware Implementation 

  * The frequency of alternating the two ports of dot matrix may need to be changed in real hardware implementation depending upon the response time and quality of the dot matrix that you may be using. The easiest way to change it would be to alter the delay in 137th line of the C code.
  * A simple voltage regulator circuit should be used to power the microcontroller
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The main.hex file must me burned (not literally) in the Atmega8 microcontroller using AVR programmer or some universal programmer that supports it.
  

 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

